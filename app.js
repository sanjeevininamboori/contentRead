var express = require('express');
var app = new express();
var bodyParser = require('body-parser');
var port = 3001;
var coverageRoutes = require('./routes/coverages');
var readContentHub = require('./utility');
var contents = readContentHub.readContentFiles();

app.use(bodyParser.json({ limit: 1024 * 1024 * 20, type: 'application/json' }));
app.use(bodyParser.urlencoded(bodyParser.urlencoded({ extended: true, limit: 1024 * 1024 * 20, type: 'application/x-www-form-urlencoding' })));
app.use('/bff', coverageRoutes);
app.listen(port,function(err){
    if(err) return;
    console.log("Server started at Port " + port );
})

module.exports.app =  app;