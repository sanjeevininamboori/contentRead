var express = require('express'),
    router = express.Router();
var coverages =  require('../controllers/coverages')    

    router.get('/coverages',coverages.fetchCoverages);

    module.exports = router;
