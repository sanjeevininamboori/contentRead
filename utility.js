const fs = require('fs');
const dir = './contents';

function readContentFiles() {
    return new Promise(function (resolve, reject) {
        var file = {};
        return readDirectoryFileNames(dir).then(function (fileNames) {
            fileNames.forEach(element => {
                var filepath = dir + '/' + element;
                var contentObjectName = element.split('.');
                var data = readJSONContent(filepath)
                file[contentObjectName[0]] = readJSONContent(filepath);
            });
        })
        resolve(Promise.all(file))
    });
}

function readDirectoryFileNames(directoryName) {
    return new Promise(function (resolve, reject) {
        fs.readdir(dir, (err, filenames) => {
            if (err)
                reject(err);
            else
                resolve(filenames)
        });
    })
}

function readJSONContent(filepath) {
    fs.readFile(filepath, function (err, contentData) {
        if (err) console.log(err);
        else
            return JSON.parse(contentData.toString())
    })
}

module.exports.readContentFiles = readContentFiles;
